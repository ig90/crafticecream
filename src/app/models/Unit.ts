export class Unit {
    id: number;
    type: string;
    capacity: number;
}