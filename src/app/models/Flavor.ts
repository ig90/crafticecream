export class Flavor {
    id: number;
    name: string;
    isFavorite: boolean;
}