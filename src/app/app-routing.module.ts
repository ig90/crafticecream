import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { UnitsComponent } from './components/units/units.component';
import { OrdersComponent } from './components/orders/orders.component';
import { FlavorsComponent } from './components/flavors/flavors.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'flavors', component: FlavorsComponent },
  { path: 'units', component: UnitsComponent },
  { path: 'orders', component: OrdersComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
