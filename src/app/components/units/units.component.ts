import { Component, OnInit } from '@angular/core';
import {Unit} from '../../models/Unit';
@Component({
  selector: 'app-units',
  templateUrl: './units.component.html',
  styleUrls: ['./units.component.scss']
})
export class UnitsComponent implements OnInit {

  units: Unit[];

  constructor() { }

  ngOnInit() {

    this.units = [
      { id: 1,
        type: 'pojemnik',
        capacity: 2
      },
      { id: 2,
        type: 'pojemnik',
        capacity: 5
      },
      { id: 3,
        type: 'kuweta',
        capacity: 10
      }
    ];
  }
}
