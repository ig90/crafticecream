import { Component, OnInit } from '@angular/core';
import { Flavor } from '../../models/Flavor';

@Component({
  selector: 'app-flavors',
  templateUrl: './flavors.component.html',
  styleUrls: ['./flavors.component.scss']
})

export class FlavorsComponent implements OnInit {

  flavors: Flavor[];
  clearInput: string;

  constructor() { }

  ngOnInit() {

    this.flavors = [
      { id: 1,
        name: 'waniliowy',
        isFavorite: false
      },
      { id: 2,
        name: 'truskawkowy',
        isFavorite: false
      },
      { id: 3,
        name: 'czekoladowy',
        isFavorite: false
      },
      { id: 4,
        name: 'pistacjowy',
        isFavorite: false
      },
    ];
  }

  onAdd(name) {
    let count = this.flavors.length;
    count++;
    this.flavors.push({id: count, name: name, isFavorite: false});
    this.clearInput = '';
  }
  onChange(course) {
    const index = this.flavors.indexOf(course);
    this.flavors.splice(index, 1);
}

  onFavoriteChanged(isFavorite) {
    console.log(isFavorite);
  }

}
